import {Component, Inject, OnInit} from '@angular/core';
import {MatDialog, MatTableDataSource, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-units',
  templateUrl: './units.component.html',
  styleUrls: ['./units.component.css']
})
export class UnitsComponent implements OnInit {
  displayedColumns = ['tools', 'position', 'name', 'description', 'date'];
  dataSource = new MatTableDataSource();

  constructor(private route: ActivatedRoute, public dialog: MatDialog) {
  }

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource(this.route.snapshot.data['units']);
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

  edition(unit: TestUnit) {

    const dialogRef = this.dialog.open(EditionDialog, {
      width: '450px',
      data: Object.assign({}, unit)
    });

    dialogRef.afterClosed().subscribe(result => {
      if (typeof result !== 'undefined') {
        unit.name = result.name;
        unit.description = result.description;
      }
    });
  }
}

@Component({
  templateUrl: './edition.dialog.html',
})
export class EditionDialog {

  constructor(public dialogRef: MatDialogRef<EditionDialog>, @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}

export interface TestUnit {
  id: number;
  name: string;
  position: number;
  description: string;
  date: string;
}
