import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {UnitsService} from './units.service';
import {TestUnit} from '../sections/units/units.component';


@Injectable()
export class UnitsResolver implements Resolve<TestUnit[]> {
  constructor(private unitsService: UnitsService) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<any> {
    return this.unitsService.getUnits();
  }
}
