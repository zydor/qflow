import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/delay';
import {TestUnit} from '../sections/units/units.component';

@Injectable()
export class UnitsService {

  constructor(private httpClient: HttpClient) {
  }

  url = 'resources/elements.json';

  getUnits(): Observable<TestUnit[]> {
    return this.httpClient.get<TestUnit[]>(this.url).delay(1000);
  }

}
