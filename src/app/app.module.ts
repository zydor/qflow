import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {
  MatButtonModule, MatCardModule, MatDatepickerModule, MatDialogModule,
  MatGridListModule, MatIconModule,
  MatInputModule, MatListModule, MatNativeDateModule,
  MatProgressBarModule, MatSidenavModule,
  MatTableModule, MatToolbarModule
} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './sections/home/home.component';
import {EditionDialog, UnitsComponent} from './sections/units/units.component';
import {ProjectsComponent} from './sections/projects/projects.component';
import {HttpClientModule} from '@angular/common/http';
import {UnitsService} from './services/units.service';
import {UnitsResolver} from './services/units.resolver';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';


const appRoutes: Routes = [
  {path: '', component: HomeComponent},
  {
    path: 'units', component: UnitsComponent,
    resolve: {
      units: UnitsResolver
    }
  },
  {path: 'projects', component: ProjectsComponent},
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    UnitsComponent,
    EditionDialog,
    ProjectsComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    RouterModule.forRoot(
      appRoutes,
      {enableTracing: true}
    ),
    FormsModule,
    ReactiveFormsModule,
    MatTableModule,
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatCardModule,
    MatGridListModule,
    MatProgressBarModule,
    MatDialogModule,
    MatInputModule
  ],
  entryComponents: [
    EditionDialog
  ],
  providers: [
    UnitsService,
    UnitsResolver
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
